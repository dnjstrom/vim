#!/bin/bash

git submodule update --init
git submodule update --recursive

rm ~/.vimrc
ln --symbolic ~/.vim/vimrc ~/.vimrc;

rm ~/.gvimrc
ln --symbolic ~/.vim/gvimrc ~/.gvimrc;
