"Pathogen
set nocompatible
runtime bundle/vim-pathogen/autoload/pathogen.vim
call pathogen#infect()

"Variables
let mapleader=","
let delimitMate_expand_cr = 1
let NERDTreeChDirMode=2
let NERDTreeMouseMode=3
let NERDTreeMinimalUI=1
let NERDTreeQuitOnOpen=1
let NERDTreeIgnore=['\~$', '\.class$', '\.swp$', '\.pyc$', '\.aux$', '\.out$', '\.aux$', '\.hi$', '.o$']
let DeleteTrailingWhitespace = 1
let DeleteTrailingWhitespace_Action = 'delete'
let SuperTabDefaultCompletionType = "context"
let g:ctrlp_cmd = 'CtrlPMixed'

"Settings
colorscheme solarized
syntax enable

set autochdir
set number
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set hlsearch		" Highlight search-terms
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden             " Hide buffers when they are abandoned
set wildmenu
set title
set ruler
set expandtab
set shiftround "Round the indention nicely with other indents
set ruler
set nowrap
set noerrorbells visualbell t_vb= "disable all errorbells

set background=light
set mouse=a		" Enable mouse usage (all modes)
set wildmode=list:longest
set wildignore=*~,*.class*,*.pdf,*.swp,*.pyc,*.aux,*.hi,*.o,*.dvi,*.ps
set scrolloff=3
set backspace=indent,eol,start
set tabstop=4
set softtabstop=4
set shiftwidth=4
set history=1000
set colorcolumn=80

if has('unnamedplus')
    set clipboard=unnamedplus
else
    set clipboard=unnamed
endif


"Platform specific settings
if has('win32')
	set backupdir=$HOME\\vimfiles\\tmp
	set directory=$HOME\\vimfiles\\tmp

	let $LANG = 'en_US' "Make sure I always get an english UI
	language messages en
else
	set backupdir=~/.vim/tmp,~/.tmp,/var/tmp,/tmp
	set directory=~/.vim/tmp,~/.tmp,/var/tmp,/tmp
    set undodir=~/.vim/tmp
    set undofile
endif

if has('gui_running')
    "This is taken care of in gvimrc
else
    set t_Co=256 "Enable more colors (default: 8)
endif


" Automatic reloading of .vimrc on save
autocmd! bufwritepost .vimrc source %
autocmd vimenter * if !argc() | NERDTree | endif

"Indent correctly
if has("autocmd")
	filetype plugin indent on

	"Jump to last edited line
	au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

    "Add some simple file associations
	au BufReadPost *.m set filetype=octave
	au BufReadPost *.repy set filetype=python
endif

"Mappings
map <F2> :NERDTreeToggle<CR>
call togglebg#map("<F5>")

nmap <F9> :SCCompile<cr>
nmap <F10> :SCCompileRun<cr>

nmap cn :cn<CR>
nmap cp :cp<CR>

"Make saving work with lazy shift
command W w
command Q q
command Wq wq
command WQ wq
