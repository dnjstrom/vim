set guioptions= "disable gui-stuff
set noerrorbells visualbell t_vb= "disable all errorbells

if has('win32')
	set guifont=Consolas:h10:cANSI
	set lines=55 columns=110
else
    set lines=40 columns=84
endif
